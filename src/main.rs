pub mod eyesheet {
    pub mod banjomus {
        tonic::include_proto!("eyesheet.banjomus");
    }
}

mod auth;

use futures::{future, StreamExt, Stream};
use jsonwebtoken::TokenData;
use rdkafka::{consumer::{StreamConsumer, Consumer}, ClientConfig, Message, error::KafkaError};
use tokio_stream::wrappers::errors::BroadcastStreamRecvError;
use tonic::{async_trait, Request, Status, Response, codegen::InterceptedService};
use std::{env, ffi::OsStr, error::Error, pin::Pin, collections::HashSet, sync::Arc};
use uuid::Uuid;
use tokio::sync::broadcast;

use crate::eyesheet::banjomus::banjomus_server::BanjomusServer;

#[derive(Clone, Debug, serde::Deserialize)]
struct TimerChangeMessage {
    only_users: Option<HashSet<Uuid>>
}

struct BanjomusImpl {
    sender: broadcast::Sender<Arc<TimerChangeMessage>>,
}

impl BanjomusImpl {
    fn get_receiver(&self) -> broadcast::Receiver<Arc<TimerChangeMessage>> {
        self.sender.subscribe()
    }

    fn create_service(kafka_config: ClientConfig) -> InterceptedService<BanjomusServer<Self>, fn(Request<()>) -> Result<Request<()>, Status>> {
        let inner = Self {
            sender: broadcast::channel(16).0,
        };

        {
            let sender = inner.sender.clone();
            let consumer: StreamConsumer = kafka_config
                .create()
                .expect("Kafka stream consumer should be created");

            consumer
                .subscribe(&["timer-change"])
                .expect("subscribing to topics should not fail");

            tokio::spawn(async move {
                consumer
                    .stream()
                    .filter_map(|result| future::ready(log_and_discard_kafka_error(result)))
                    .for_each(|message| {
                        let sender = sender.clone();
                        async move {
                            message.payload()
                                .map(serde_json::from_slice::<TimerChangeMessage>)
                                .map_or_else(
                                    || std::eprintln!("Received message without valid payload"),
                                    |payload| match payload {
                                        Ok(payload) => { sender.send(Arc::new(payload)).unwrap_or(0); },
                                        Err(err) => std::eprintln!("Received malformed message payload: {err}"),
                                    });
                        }
                    })
                    .await;
            });
        }

        BanjomusServer::with_interceptor(inner, |mut request| {
            auth::insert_token_in_extensions(&mut request)?;
            Ok(request)
        })
    }
}

#[async_trait]
impl eyesheet::banjomus::banjomus_server::Banjomus for BanjomusImpl {
    type ListenForTimerChangeEventsStream = Pin<Box<dyn Stream<Item = Result<(), Status>> + Send>>;

    async fn listen_for_timer_change_events(&self, request: Request<()>) -> Result<Response<Self::ListenForTimerChangeEventsStream>, Status>  {
        let token = request
            .extensions()
            .get::<TokenData<auth::TokenClaims>>();
            //.ok()_or(tonic::Status::unauthenticated("No auth token supplied"))?;
        let user: Option<Uuid> = token.map(|token| token.claims.sub.parse())
            .transpose()
            .map_err(|err| {
                std::eprintln!("Received invalid token. Failure during parsing of sub uuid: {err:?}");
                Status::unauthenticated("Invalid Authorization header")
            })?;

        let stream = tokio_stream::wrappers::BroadcastStream::new(self.get_receiver())
            .filter_map(move |message| async move {
                message.map_or_else(
                    |BroadcastStreamRecvError::Lagged(missed)| {
                        std::eprintln!("Receiver lagged behind {missed} messages");
                        None
                    },
                    |message| {
                        // !has_only_users => send
                        // has_only_users && !has_user => ignore
                        // has_only_users && has_user => user in only_users ? send : ignore
                        message.only_users.as_ref().map_or(true, |only_users| {
                            user.as_ref().map_or(false, |user| only_users.contains(user))
                        }).then_some(Ok(()))
                    })
        });
        Ok(Response::new(Box::pin(stream)))
    }

}

fn log_and_discard_kafka_error<T>(result: Result<T, KafkaError>) -> Option<T> {
    if let Err(error) = &result {
        std::eprintln!("Kafka error: {error}");
    };
    result.ok()
}

#[derive(Debug)]
struct EnvVarNotUnicode;

impl std::fmt::Display for EnvVarNotUnicode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "environment variable was not unicode")
    }
}

impl Error for EnvVarNotUnicode {}

fn env_var_or<K: AsRef<OsStr>>(key: K, default: impl Into<String>) -> Result<String, EnvVarNotUnicode>{
    use env::VarError::*;
    match env::var(key) {
        Ok(val) => Ok(val),
        Err(NotPresent) => Ok(default.into()),
        Err(NotUnicode(_)) => Err(EnvVarNotUnicode),
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Sync + Send>> {
    println!("Hello, world!");
    
    let mut kafka_config = ClientConfig::new();
    kafka_config.set("bootstrap.servers", env_var_or("KAFKA_URL", "localhost")?);
    kafka_config.set("group.id", "banjomus");
    kafka_config.set("enable.partition.eof", "false");

    tonic::transport::Server::builder()
        .accept_http1(true)
        .layer((
                tower_http::cors::CorsLayer::very_permissive(),
                tonic_web::GrpcWebLayer::new(),
              ))
        .add_service(BanjomusImpl::create_service(kafka_config))
        .serve(env::var("BANJOMUS_SERVICE_ADDR")?.parse()?)
        .await?;

    Ok(())
}
